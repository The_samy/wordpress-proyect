<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'public' );

/** MySQL database username */
define( 'DB_USER', 'public' );

/** MySQL database password */
define( 'DB_PASSWORD', 'public' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         '3:F.U+?VB8) O.3|Foh}nU ;LzF@I!!nIHlOeVyT?;,T7y6 t~x*O(~(,7[[$sa&' );
define( 'SECURE_AUTH_KEY',  'P6:)tKAp`Ga8o~Ch7cOA,#a7_f{$AieUpk?DG!?Z6%Pzd@7Oz7B{&!C|& wKPKwz' );
define( 'LOGGED_IN_KEY',    '&v4VK fnX=w1)_s$|9?-L7 /un2)i]/%Yh7t!TC9U;V@mp[]*[PER@UJOe){fB)&' );
define( 'NONCE_KEY',        ')sL=oP0wIm/<,?k<hPCEMAb u(/Jx:y]H61Az`CXf5goLrpE?fQJ|&jZw;9wGYr/' );
define( 'AUTH_SALT',        'WQ@42$[82x+_-dS gMM+ft0OjK,W:,lK:/js]+K^%vcWKKp+-;ccnBQCH)!q-^++' );
define( 'SECURE_AUTH_SALT', 'HK#GgZXbuKCU]#tCy;1<*u-_P<&8))(h#)#8^ASh;2~LcI_!JWa5ESm-}:A}ja`.' );
define( 'LOGGED_IN_SALT',   'Bth+$#lo0NCRF$T T@*9$*G6;D[toGSyNU}U=.d<beHD.]X8`vRal.%;Tx!t8b Y' );
define( 'NONCE_SALT',       'Co.k)r-lW*)($s!1!Y.xkz@T=cWBqi6F;BXwTC]2q+5hZ*q;HCEHBmX6nh!hZJ$o' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
