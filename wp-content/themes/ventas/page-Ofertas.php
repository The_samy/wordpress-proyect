<?php

get_header();

while(have_posts()):
    the_post();
?>
  

  <div style="margin:auto"; class="col-lg-4 col-md-6 mb-4">
          
            
            
                <h4 class="card-title">
                  <a href="#"><?php  the_title(); ?></a>
                </h4>
               
                <p class="card-text"><?php echo the_content(); ?></p>
            
              </div>
       </div>
  </div>
  </div>
       <?php 

endwhile;

get_footer();
?>