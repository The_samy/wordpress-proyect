
<?php get_header();

while(have_posts()):
    the_post();


?>




<div style="margin:auto"; class="col-lg-4 col-md-6 mb-4">
            <div class="card h-100">
              <a href="#"><img class="card-img-top" src="<?php  echo the_field('imagen')   ?>" alt=""></a>
              <div class="card-body">
                <h4 class="card-title">
                  <a href="#"><?php  the_title(); ?></a>
                </h4>
                <h5>Precio:
               <?php the_field('precio'); ?></h5>
               <h6>Proveedor:  <?php the_field('proveedor'); ?></h5></h6>
               <h6> WhatsApp:  <?php the_field('telefono'); ?></h5></h6>
                <p class="card-text"><?php echo wp_trim_words(get_the_content(),25); ?></p>
  
              </div>
                    <div class="card-footer">
                          <small class="text-muted">&#9733; &#9733; &#9733; &#9733; &#9734;</small>
                     </div>

            </div>

          </div>
  <?php 
  
endwhile;
  
  
  get_footer(); ?>
